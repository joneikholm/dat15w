import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by joneikholm on 11/09/15.
 */
public class MenuRocks extends Application
{
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox vBox = new VBox();
        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        MenuItem openItem = new MenuItem("Open");
        openItem.setOnAction(event -> {
            System.out.println("you pressed OPEN");
        });
        CheckMenuItem checkMenuItem = new CheckMenuItem("show lines?");

        fileMenu.getItems().add(checkMenuItem);
        vBox.getChildren().add(menuBar);
        menuBar.getMenus().add(fileMenu);
        fileMenu.getItems().add(openItem);
        Scene scene = new Scene(vBox, 400,400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
