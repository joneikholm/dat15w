package mvc;

/**
 * Created by joneikholm on 12-10-15.
 */
public class Controller
{
    MyNotesView myNotesView;
    Model model;
    public Controller(MyNotesView myNotesView, Model model){
        this.myNotesView = myNotesView;
        this.model=model;
    }

    public void saveButtonAction(String key, String value){
        if(key!=null && key.length()>0){
            if(value!=null && value.length()>0){
                model.add(key,value);
                myNotesView.updateStatusMessage("New note added.");
                myNotesView.updateButtons();
                myNotesView.clearCenter();

                return; // to get out of the inner if
            }
        }
        myNotesView.updateStatusMessage("Please enter both values in both textfields");

    }

}
