package mvc;

/**
 * Created by joneikholm on 27-11-15.
 */
public class SuperPrinter
{
    public static void main(String[] args)
    {
        new SuperPrinter();
    }

    public SuperPrinter(){

        Note note1 = new Note("HL", "msg",this);// this refers to note1
        Note note2 = new Note("HL2", "msg2",this);// this refers to note2

    }

    public void printNote(Note note){
        System.out.println(note.key + " "+ note.value);
    }
}
