package mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by joneikholm on 12-10-15.
 */
public class Model
{
    TreeMap<String, String> map = new TreeMap<>();  //start with empty map for now

    public void add(String key, String value){
        map.put(key,value);
    }


    public List<Note> getNotes(){

        ArrayList<Note> list=new ArrayList<>();
        for(Map.Entry<String, String> note:map.entrySet()){
            list.add(new Note(note.getKey(), note.getValue()));
        }
        return list;
    }
}
