package mvc;

/**
 * Created by joneikholm on 12-10-15.
 */
public class Note
{
    public String key;
    public String value;
    public SuperPrinter superPrinter;
    public Note(String key, String value, SuperPrinter superPrinter){
        this.key=key;
        this.value=value;
        this.superPrinter = superPrinter;
        superPrinter.printNote(this);
    }
    public Note(String key, String value){
        this.key=key;
        this.value=value;
        this.superPrinter = superPrinter;

    }


}
