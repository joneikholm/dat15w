package mvc;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;
import java.util.TreeMap;

/**
 * Created by joneikholm on 05-10-15.
 */
public class MyNotesView extends Application
{

    public BorderPane borderPane;
    VBox leftColumnButtonsVBox;
    VBox centerContentVBox;
    TextField titleField;
    TextArea textArea;
    Label statusMessageLabel = new Label("statusMessageLabel");
    TreeMap<String, String> map = new TreeMap<>();  //start with empty map for now
    Controller controller;
    Model model;
    public static void main(String[] args) {
        launch(args);
    }

    public MyNotesView(){
        model = new Model();
        controller=new Controller(this, model);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        borderPane = new BorderPane();
        leftColumnButtonsVBox= new VBox(3);
        centerContentVBox= new VBox(3);
        titleField = new TextField();
        textArea = new TextArea();
        this.borderPane.getClip();
        Button saveButton = new Button("Save");
        centerContentVBox.getChildren().addAll(titleField,textArea,saveButton, statusMessageLabel);
        saveButton.setOnAction(event -> {
            // we call the controller, asking to handle the saveButton click
            controller.saveButtonAction(titleField.getText(), textArea.getText());
        });
        borderPane.setLeft(leftColumnButtonsVBox);
        // + button, for creating a new entry
        Button newNoteButton = new Button("New Note");
        newNoteButton.setOnAction(event -> {
            titleField.clear();
            textArea.clear();
            borderPane.setCenter(centerContentVBox);
        });
        borderPane.setTop(newNoteButton);

        Scene scene = new Scene(borderPane,450,600);
        primaryStage.setTitle("My Cool Note App");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public void updateButtons(){
        leftColumnButtonsVBox.getChildren().clear();
        List<Note> notes = model.getNotes();

        for(Note note : notes){
            Button entryButton = new Button(note.key);
            entryButton.setOnAction(event1 -> {
                // somehow get the text which corresponds to the "key", which was
                // the text on the button
                titleField.setText(note.key);
                textArea.setText( note.value);
                borderPane.setCenter(centerContentVBox); // fills center area
            });
            leftColumnButtonsVBox.getChildren().add(entryButton);
        }
    }

    public void updateStatusMessage(String message)
    {
        statusMessageLabel.setText(message);
    }

    public void clearCenter()
    {
        borderPane.setCenter(null); // clears center area

    }
}
