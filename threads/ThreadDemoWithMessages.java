package threads;

import java.util.ArrayList;

/**
 * Created by joneikholm on 09-10-15.
 */
public class ThreadDemoWithMessages
{
    // make a car race, with 5 cars.
    // Each car will sleep for a random time, then drive one lap
    // The first car to reach 5 laps wins.

    ArrayList<RaceCarWithMessage> cars = new ArrayList<>();
    public static void main(String[] args)
    {
        // make the 4 other cars stop if one car wins
        // get a refence to each RaceCarWithMessage object
        // when one car wins, it should call all the other
        // cars raceIsOver()
             new ThreadDemoWithMessages();
    }
    public ThreadDemoWithMessages(){
        cars.add(new RaceCarWithMessage("Ford Ka",this));
        cars.add(new RaceCarWithMessage("Lamborghini", this));
        cars.add(new RaceCarWithMessage("Toyota", this));
        cars.add(new RaceCarWithMessage("Volkswagen", this));
        cars.add(new RaceCarWithMessage("Fiat", this));
        for(RaceCarWithMessage car:cars){
            Thread thread = new Thread(car);
            thread.start();
        }

    }
    public void stopCars(){
        for (int i = 0; i <cars.size() ; i++)
        {
           cars.get(i).raceIsOver();
        }
    }
}
