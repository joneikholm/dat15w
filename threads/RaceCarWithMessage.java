package threads;

import java.util.Random;

/**
 * Created by joneikholm on 09-10-15.
 */
public class RaceCarWithMessage implements Runnable
{

    String name="";
    ThreadDemoWithMessages threadDemoWithMessages;
    boolean raceIsOver=false;
    public RaceCarWithMessage(String name, ThreadDemoWithMessages threadDemoWithMessages)
    {
        this.threadDemoWithMessages = threadDemoWithMessages;
        this.name=name;
    }

    public void raceIsOver(){
        raceIsOver=true;
    }
    @Override
    public void run()
    {
        Random random= new Random();
        int laps=1;
        String stars="";
        while (laps<=5 && !raceIsOver){
            stars = stars + "*****";
            System.out.println(stars+ ": " +name);
            try{
                Thread.sleep(random.nextInt(2000));
            }catch (InterruptedException e){

            }
            laps++;
        }
        if(!raceIsOver){
            System.out.println(name + " won !") ;
            threadDemoWithMessages.stopCars();
        }
        // now this car won:
        // call all the other car's raceIsOver()


    }
}
