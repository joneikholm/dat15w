package threads;

import java.util.Random;

/**
 * Created by joneikholm on 09-10-15.
 */
public class RaceCar implements Runnable
{

    String name="";
    public RaceCar(String name){
        this.name=name;
    }

    public void raceIsOver(){

    }
    @Override
    public void run()
    {
        Random random= new Random();
        int laps=1;
        String stars="";
        while (laps<=5 && ThreadDemo.place<1){
            stars = stars + "***";
            System.out.println(stars+ ": " +name);
            try{
                Thread.sleep(random.nextInt(2000));
            }catch (InterruptedException e){

            }
            laps++;
        }
        // now this car won:
        // call all the other car's raceIsOver()
        ThreadDemo.place++;
        System.out.println(name + " is number:"+ThreadDemo.place) ;
    }
}
