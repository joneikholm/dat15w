package threads;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by joneikholm on 09-10-15.
 */
public class WebCrawler extends Application
{

    public String url = "";
    public String searchWord = "";
    Label resultLabel;
    Button startButton;
    private Crawler crawler = null;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        // make GUI with:
        // 1 textField for entering URL
        // 1 textField for entering search string
        crawler = new Crawler(this);
        VBox vBox = new VBox(5);
        HBox hBox1 = new HBox(5);
        Label urlLabel = new Label("Enter URL");
        TextField urlField = new TextField();
        urlField.setPromptText("enter URL");
        Button urlButton = new Button("Add");
        urlButton.setOnAction(event -> url = urlField.getText());

        hBox1.getChildren().addAll(urlLabel, urlField, urlButton);

        HBox hBox2 = new HBox(5);
        Label searchLabel = new Label("Enter search word");
        TextField searchField = new TextField();
        searchField.setPromptText("enter search word");
        Button searchButton = new Button("Add word");
        searchButton.setOnAction(event -> searchWord = searchField.getText());

        hBox2.getChildren().addAll(searchLabel, searchField, searchButton);

         startButton = new Button("Start Crawling");
        startButton.setOnAction(event -> startCrawling());
        Button stopButton = new Button("Stop Crawling");
        stopButton.setOnAction(event -> stopCrawling());

        Button launchWebsiteButton= new Button("Launch website");
        launchWebsiteButton.setOnAction(event -> launchWebsite());

        resultLabel = new Label("Result will come");
        vBox.getChildren().addAll(hBox1, hBox2, startButton, stopButton, launchWebsiteButton, resultLabel);

        Scene scene = new Scene(vBox, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private void launchWebsite(){
        try
        {
            Desktop.getDesktop().browse(new URI("http://"+url));
        } catch (IOException e1)
        {
            e1.printStackTrace();
        } catch (URISyntaxException e1)
        {
            e1.printStackTrace();
        }
    }

    public void receiveMessageFromCrawler(String message)
    {
        Platform.runLater(new Runnable()
        {
            @Override
            public void run()
            {
                resultLabel.setText(message);
            }
        });
    }

    private void stopCrawling()
    {
        crawler.stopRunning();
        startButton.setDisable(false);
    }

    private void startCrawling()
    {
        startButton.setDisable(true);
        crawler.setStopRunning(false);
        Thread thread = new Thread(crawler);
        thread.start();

    }
}
