package threads;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by joneikholm on 09-10-15.
 */
public class Crawler implements Runnable
{

    private WebCrawler webCrawler;
    private boolean stopRunning=false;
    public Crawler(WebCrawler webCrawler){
        this.webCrawler=webCrawler;
    }

    public void setStopRunning(boolean value){
        stopRunning=value;
    }

    @Override
    public void run()
    {
        // do some crawling
        while (!stopRunning)
        {
            doSomeCrawling();
            try
            {
                Thread.sleep(10000);
            }catch (InterruptedException e){

            }
        }

    }

    public void stopRunning(){
        stopRunning=true;
    }
    private void doSomeCrawling()
    {
        String out = "empty";
        try
        {
            URL url = new URL("http://"+webCrawler.url);
            Scanner scanner = new Scanner(url.openStream(), "UTF-8");
            out = scanner.useDelimiter("\\A").next();

        } catch (MalformedURLException e)
        {
            System.out.println("error in URL " + e.getMessage());
        } catch (IOException e)
        {
            System.out.println("IO error in URL " + e.getMessage());
        }
        if(out.contains(webCrawler.searchWord)){
            webCrawler.receiveMessageFromCrawler("Found: "+webCrawler.searchWord);
        }else {
            webCrawler.receiveMessageFromCrawler("Did not find: "+webCrawler.searchWord);

        }
        System.out.println("Just crawled for "+webCrawler.searchWord + " url:"+webCrawler.url);
    }
}
