package threads;

/**
 * Created by joneikholm on 09-10-15.
 */
public class ThreadDemo
{
    public static int place=0;
    // make a car race, with 5 cars.
    // Each car will sleep for a random time, then drive one lap
    // The first car to reach 5 laps wins.
    public static void main(String[] args)
    {
        // make the 4 other cars stop if one car wins
        // get a refence to each RaceCar object
        // when one car wins, it should call all the other
        // cars raceIsOver()
        Thread thread1 = new Thread(new RaceCar("Ford Ka"));
        Thread thread2 = new Thread(new RaceCar("Lamborghini"));
        Thread thread3 = new Thread(new RaceCar("Toyota"));
        Thread thread4 = new Thread(new RaceCar("Volkswagen"));
        Thread thread5 = new Thread(new RaceCar("Fiat"));
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
    }
}
