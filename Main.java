import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
// Love yourself and the code. Share.
// Never stop improving
public class Main extends Application
{
    Scene scene1;
    private boolean documentIsSaved=false;
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setOnCloseRequest(event1 -> {
            event1.consume();  // here we STOP it from propagating
            if(documentIsSaved){  // now it's safe to close the application
                primaryStage.close();
            }
        });
        BorderPane borderPane = new BorderPane();
        VBox vBox = new VBox();// Layout
        borderPane.setRight(vBox);
        Label label = new Label();
        TextField textField = new TextField();
        Button loginButton = new Button("Save document");
        loginButton.setOnAction(event -> {
            documentIsSaved=true;
            label.setText("You have saved the document.");
        });
        loginButton.setStyle("-fx-font-size: 20");
        vBox.getChildren().addAll(label, textField, loginButton);
        scene1 = new Scene(borderPane,600,400);
        primaryStage.setScene(scene1);
        primaryStage.show();
    }
}
