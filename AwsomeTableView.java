import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by joneikholm on 11/09/15. asdf
 */
public class AwsomeTableView extends Application
{

    private ObservableList<Product> products=null;
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox vBox = new VBox();
        Scene scene = new Scene(vBox,300,450);
        TextField nameField= new TextField();
        nameField.setPromptText("name");
        TextField priceField= new TextField();
        priceField.setPromptText("price");
        Button addButton = new Button("Add");
        Button deleteButton = new Button("Delete");
        TableView<Product> tableView = new TableView<>();
        addButton.setOnAction(event -> {
            try {
                double price = Double.parseDouble(priceField.getText());
                Product p = new Product(nameField.getText(), price);
                products.add(p);
                // update database with this new Object
                // update the other table, by either giving the new Product object as argument
                // OR querying the database.
                nameField.clear();
                priceField.clear();
            }catch (NumberFormatException e){
                System.out.println("error in converting: "+e.getMessage());
            }
        });

        deleteButton.setOnAction(event -> {
            products.remove(tableView.getSelectionModel().getSelectedIndex());
        });

        TableColumn<Product, String> nameColumn = new TableColumn();
        nameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        nameColumn.setText("Name:");
        TableColumn<Product, Double> priceColumn = new TableColumn<>();
        priceColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("price"));
        priceColumn.setText("Price:");

        tableView.getColumns().addAll(nameColumn,priceColumn);
        makeTheList();
        tableView.setItems(products);
        vBox.getChildren().addAll(tableView, nameField, priceField, addButton, deleteButton);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void makeTheList(){
        products = FXCollections.observableArrayList();
        products.add(new Product("Roller Blades", 500.00));
        products.add(new Product("Helmet", 750.00));
        products.add(new Product("Knee Pads", 400.00));
        products.add(new Product("Water Bottle", 100.00));
        products.add(new Product("Tennis Ball", 45.50));

    }
}
