package exceptions;

/**
 * Created by joneikholm on 05-10-15.
 */
public class UseExceptions
{

    public static void main(String[] args) {
        UseExceptions useExceptions = new UseExceptions();
    }

    public UseExceptions(){

        //System.out.println(divide(5,0));
        try {
            mysteryMethod();
        } catch (Exception e) {
            System.out.println("something went wrong..." +e.getMessage());
        }
        System.out.println("Done");
    }

   private void mysteryMethod() throws Exception{
       int a=2/0;
   }

   private void printPerson(Person person){
       try {
           System.out.println(person.toString());
       }catch (NullPointerException e){
           System.out.println("person object was null" + e.getMessage());
       }
   }

    private int divide(int a, int b){
        int result=0;
        try {
            result= a / b;  // will throw ArithmeticException when b==0
        }catch (ArithmeticException ae){
            System.out.println("AE execp. You probably divived by zero. " + ae.getMessage());
        }
        catch (Exception e){
            System.out.println("Excep. You probably divived by zero. " + e.getMessage());
        }finally {
            // if you want something to occur, NO MATTER WHAT
        }
        return result;
    }
}
