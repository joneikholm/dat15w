import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created by joneikholm on 14/09/15.
 */
public class LoginFX extends Application
{
    private BorderPane borderPane;
    private Label messageLabel=new Label("Answer from server:");
    public static void main(String[] args) {
        launch(args);
    }

    public void updateLabel(String msg){
        Platform.runLater(new Runnable()
        {
            @Override
            public void run() {
                messageLabel.setText(messageLabel.getText()+msg);
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        borderPane = new BorderPane();
        VBox vBox = new VBox(5);
        TextField nameField = new TextField();
        TextField passwordField = new TextField();
        nameField.setPromptText("enter name");
        passwordField.setPromptText("enter password");
        Button loginButton = new Button("Login");
        loginButton.setOnAction(event -> {
            // handle login, by contacting server
            Worker worker = new Worker(this);
            Thread thread = new Thread(worker);
            System.out.println("before thread start");
            thread.start();
        });
        vBox.getChildren().addAll(nameField, passwordField, loginButton, messageLabel);
        borderPane.setTop(vBox);
        Scene scene = new Scene(borderPane, 400,400);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
