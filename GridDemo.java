import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by joneikholm on 31/08/15.  Do it now. Love it. Always
 */
public class GridDemo extends Application
{

    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //1. Layout
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(5, 5, 5, 10));
        gridPane.setGridLinesVisible(true);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        Button button0 = new Button("0");
        button0.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        Button button1 = new Button("1");
        Button button2 = new Button("2");
        Button button3 = new Button("3");
        gridPane.add(button1, 0, 0);// add button1 at column 0, and row 0
        gridPane.add(button2, 1, 0);// add button2 at column 1, and row 0
        gridPane.add(button3, 2, 0);
        //add button0 at col 0,row 1 and fill 2 cols, and 1 row
        gridPane.add(button0, 0, 1, 2, 1);

        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().addAll("Car", "Bike", "Train");
        comboBox.setOnAction(event -> {
            System.out.println("you selected" + comboBox.getSelectionModel().getSelectedItem().toString());
        });
        ArrayList<String> arrayList = new ArrayList<>();
        File folder = new File("./src");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            File temp=listOfFiles[i];
            if (temp.isFile() && temp.getName().indexOf("jpg")>0) {
                    arrayList.add(temp.getName());
                System.out.println("File " + temp.getName());
            }
        }

        ListView<String> listView = new ListView<>();
        ObservableList<String> myList= FXCollections.observableArrayList();

        myList.addAll("a", "b", "c");


        listView.getItems().setAll(arrayList);
       // listView.setItems(myList);
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setOnMouseClicked(event3 -> {
            System.out.println("you clicked on an item!!" + listView.getSelectionModel().getSelectedItem().toString());
        });
      //  listView.getItems().addAll("Apple", "Raisin", "Banana", "Grape", "Orange");
        TextField inputField=new TextField();
        inputField.setPromptText("add some product");
        Button addButton = new Button("Add item");
        addButton.setOnAction(event2 -> {
            listView.getItems().add(inputField.getText());
            inputField.clear();
        });
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(event1 -> {
            ObservableList<String> selected = listView.getSelectionModel().getSelectedItems();
            int numberOfSelected=selected.size();
            System.out.println("selected: "+ numberOfSelected + "items to delete");

            for (int i = 0; i < numberOfSelected; i++) {
                System.out.println("about to delete item: "+i);
                listView.getItems().remove(selected.get(0));

            }
        });
        Button listButton = new Button("View selected items");
        listButton.setOnAction(event -> {
            ObservableList<String> selected = listView.getSelectionModel().getSelectedItems();
            int numberOfSelected = selected.size();
            for (int i = 0; i < numberOfSelected; i++) {
                System.out.println(selected.get(i).toString() + ", ");

            }
        });
        gridPane.add(comboBox, 0, 2);
        gridPane.add(listView, 0, 3);
        gridPane.add(listButton, 0, 4);
        gridPane.add(addButton,1,5);
        gridPane.add(inputField,0,5);
        gridPane.add(deleteButton,0,6);

        Image image = new Image("nature1.jpg");
        ImageView imageView = new ImageView(image);
        imageView.setX(400);
        imageView.setPreserveRatio(true);
        imageView.fitWidthProperty().bind(gridPane.widthProperty());
        imageView.fitHeightProperty().bind(gridPane.heightProperty());
        gridPane.add(imageView,0,7);




        //2. Scene
        Scene scene = new Scene(gridPane, 400,300);

        //3. set scene and show()
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
