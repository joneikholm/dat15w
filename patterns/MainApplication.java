package patterns;

/**
 * Created by joneikholm on 16-10-15.
 */
public class MainApplication
{
    public static void main(String[] args)
    {
        Database dbMYSQL = Database.getInstance("MySQL");
        dbMYSQL = Database.getInstance("Oracle");

        System.out.println(dbMYSQL.name);
    }
}
