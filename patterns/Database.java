package patterns;

/**
 * Created by joneikholm on 16-10-15.
 */
public class Database
{

    public String name;
    private static Database database = null;

    private Database(String name){
        this.name=name;
        database = this;
    }
    public static synchronized Database getInstance(String name){

        if(database==null){
            // CPU stops this thread
            database = new Database(name);
        }
        return database;
    }
}
