package patterns;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.HashSet;

/**
 * Created by joneikholm on 16-10-15.
 */
public class ChannelButtons extends Application
{
    private HBox hBox = new HBox(5);
    public static void main(String[] args)
    {
        HashSet<Integer> hash= new HashSet<>();
        hash.removeAll(null);
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Scene scene = new Scene(hBox, 500,500);
        Mediator mediator = new Mediator();
        for (int i = 1; i <=5 ; i++)
        {
            Button button = new Button("Channel "+i);
            mediator.addButton(button);
            button.setOnAction(event -> mediator.handleButtonClick(button));
            hBox.getChildren().add(button);
        }
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
