package patterns;

import javafx.scene.control.Button;

import java.util.ArrayList;

/**
 * Created by joneikholm on 16-10-15.
 */
public class Mediator
{
    private ArrayList<Button> buttons = new ArrayList<>();

    public void addButton(Button button){
        buttons.add(button);
    }

    public void handleButtonClick(Button button){

        for(Button btn : buttons){
            if(btn.equals(button)){
                btn.setDisable(true);
            }else{
                btn.setDisable(false);
            }

        }
    }

}
