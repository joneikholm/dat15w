package database;

import java.sql.*;

/**
 * Created by joneikholm on 25-10-15.
 */
public class SimpleDBdemo
{
    //  Database credentials
    private Connection conn=null;

    public static void main(String[] args)
    {
        new SimpleDBdemo();
    }

    public SimpleDBdemo(){

            try
            {
                String DB_URL = "jdbc:mysql://localhost:8889/dat15w";
                String USER = "root";
                String PASS = "root";
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                	System.out.println("conn obj created"+conn + " message: ");

                //writeToDB("persons", "per", "123");
                //deleteFromDB("persons", 4);
                updateInDB("persons", 3, "Ragnhild", "goodday");
               // System.out.println(getPerson("anna"));

            } catch (SQLException e)
            {
                System.out.println("db error"+e.getMessage());
            }
    }

    private void updateInDB(String table, int id, String changedName, String newPassword)
    {
        String sql="UPDATE "+table+" SET name=?, password=? WHERE id = ?";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, changedName);
            preparedStatement.setString(2, newPassword);
            preparedStatement.setInt(3, id);
            int numberOfRows= preparedStatement.executeUpdate();
            System.out.println("Completed update. Number of rows affected:"+numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void deleteFromDB(String table, int id)
    {
        String sql="DELETE FROM persons WHERE id = ?";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int numberOfRows= preparedStatement.executeUpdate();
            System.out.println("Completed delete. Number of rows affected:"+numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void writeToDB(String table, String name, String pw){
        String sql="INSERT INTO persons VALUES (null, ?, ?)";

        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, pw);
           int numberOfRows= preparedStatement.executeUpdate();
            System.out.println("Completed insert. Number of rows affected:"+numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public String getPerson(String name){
        String out="";
        try
        {
            String sql="SELECT * FROM persons WHERE name = ?";

            //Statement statement=conn.createStatement();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                out = resultSet.getString(1);
                out += " - "+resultSet.getString(2);
                out += " - "+resultSet.getString(3);

            }else {
                out =" no data";
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        }

//
        return out;
    }
}
