package database;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.sql.*;

/**
 * Created by joneikholm on 25-10-15.
 */
public class NanoFaceBook extends Application
{
    private String username=null;
    private Connection conn = null;
    BorderPane borderPane;
    VBox loginVBox;
    VBox appointmentCountVBox;
    private VBox newsFeed = new VBox(4);
    private ScrollPane newsFeedPane = new ScrollPane(newsFeed);
    private int counter=0;
    private Button logoutButton = new Button("Log out");
    private VBox addFriendVBox = new VBox(5);

    public static void main(String[] args)
    {
        launch(args);
    }

    public NanoFaceBook()
    {

        try
        {
            String DB_URL = "jdbc:mysql://localhost:8889/dat15w";
            String USER = "root";
            String PASS = "root";
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("conn obj created" + conn + " message: ");
        } catch (SQLException e)
        {
            System.out.println("db error" + e.getMessage());
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        // fill some news
        for (counter = 0; counter <36 ; counter++)
        {
            final int number = counter;  // replace counter with post id (from database)
            VBox post = new VBox(3);
            TextField comment = new TextField();
            comment.setStyle("-fx-font-size: 8pt");
            comment.setOnAction(event -> {
                System.out.println("message sent to server " + comment.getText() + " on post nr: "+number);
            });
            post.getChildren().add(new Text("Message nr. "+counter));
            MyButton likeButton = new MyButton("Like");
            likeButton.id=counter;
            likeButton.setOnAction(event -> {
                MyButton button = (MyButton)event.getSource();
                System.out.println("you liked the text: Message nr." + number + " buttonId:"+button.id);
            });
            likeButton.setStyle("-fx-font-size: 6pt");

            // if any replies, the add them here:
            Text text = new Text("Reply: this is outrageous!");
            text.setStyle("-fx-font-size: 8pt");
            Text numberOfLikes = new Text("x likes");
            numberOfLikes.setStyle("-fx-font-size: 8pt");
            post.getChildren().addAll(numberOfLikes,text);
            post.getChildren().addAll(likeButton, comment);
            newsFeed.getChildren().add(post);
        }

        TextField searcFriendField = new TextField();
        searcFriendField.setOnAction(event2 -> {
            System.out.println("you searched...");
            MyTextField textField = (MyTextField)event2.getSource();


        });
        searcFriendField.setPromptText("Add friend...");
        Button searchButton = new Button("Add");
        TextField newPostField = new TextField();
        newPostField.setPromptText("Post something...");
        Button postButton = new Button("Post");
        postButton.setOnAction(event1 -> {

            VBox vBox = new VBox(3);
            Button likeButton = new Button("Like");
            likeButton.setStyle("-fx-font-size: 6pt");
            vBox.getChildren().addAll(new Text(newPostField.getText()), likeButton);
                    newsFeed.getChildren().add(0, vBox);
        });

        addFriendVBox.getChildren().addAll(searcFriendField, searchButton, newPostField, postButton);

        borderPane = new BorderPane();
        loginVBox = new VBox(5);
        TextField nameTextField = new TextField();
        nameTextField.setPromptText("enter username");
        TextField passwordTextField = new TextField();
        passwordTextField.setPromptText("type password");
        Button loginButton = new Button("Login");
        loginButton.setOnAction(event -> {
            // check with the database, if the user exists, and the password was correct.

            // 1. send both usrname + password to DB
            // 2. if one row is returned, then the user is validated.
            String sql = "SELECT * FROM persons WHERE name = ? and password = ?";
            try
            {
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setString(1, nameTextField.getText());
                preparedStatement.setString(2, passwordTextField.getText());
                preparedStatement.executeQuery();
            } catch (SQLException e)
            {
                e.printStackTrace();
            }
            HBox hBox = new HBox(5);
            hBox.getChildren().addAll(new Text("User: Mr. Sørensen is logged in."), logoutButton);
            borderPane.setTop(hBox);
            borderPane.setCenter(newsFeedPane);
        });

        loginVBox.getChildren().addAll(nameTextField, passwordTextField, loginButton);
        borderPane.setTop(loginVBox);

        Label rightLabel=new Label("Friends:");
        borderPane.setMargin(addFriendVBox, new Insets(12,12,12, 40));
        borderPane.setMargin(rightLabel, new Insets(40,12,12, 40));
        borderPane.setLeft(addFriendVBox);
        borderPane.setRight(rightLabel);
        Scene scene = new Scene(borderPane, 600, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    private void updateAppointmentCount()
    {
        String sql = "SELECT name, count(doctorbooking.idDoctor) FROM doctorbooking\n" +
                "INNER JOIN doctors ON\n" +
                "doctors.idDoctor = doctorbooking.idDoctor\n" +
                "GROUP BY doctorbooking.idDoctor;";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();
            appointmentCountVBox.getChildren().clear();
            Label label = new Label( "Name: \t\t\t\t Number of appointments:");
            appointmentCountVBox.getChildren().add(label);
            while (resultSet.next())
            {
                 label = new Label(resultSet.getString(1) + " \t\t\t\t" + resultSet.getInt(2));
                appointmentCountVBox.getChildren().add(label);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    private void updateInDB(String table, int id, String changedName, String newPassword)
    {
        String sql = "UPDATE " + table + " SET name=?, password=? WHERE id = ?";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, changedName);
            preparedStatement.setString(2, newPassword);
            preparedStatement.setInt(3, id);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed update. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void deleteFromDB(String table, int id)
    {
        String sql = "DELETE FROM persons WHERE id = ?";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed delete. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void insertBooking(String name, String date, int docId)
    {
        String sql = "INSERT INTO doctorbooking VALUES (null, ?, ?, null, ?)";

        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, date);
            preparedStatement.setInt(3, docId);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed insert. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void writeToDB(String table, String name, String pw)
    {
        String sql = "INSERT INTO persons VALUES (null, ?, ?)";

        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, pw);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed insert. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public String getPerson(String name)
    {
        String out = "";
        try
        {
            String sql = "SELECT * FROM persons WHERE name = ?";

            //Statement statement=conn.createStatement();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
            {
                out = resultSet.getString(1);
                out += " - " + resultSet.getString(2);
                out += " - " + resultSet.getString(3);

            } else
            {
                out = " no data";
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return out;
    }

    /*public ArrayList<Doctor> getDoctors()
    {
        ArrayList<Doctor> list = new ArrayList<>();
        try
        {
            String sql = "SELECT * FROM doctors";

            //Statement statement=conn.createStatement();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                Doctor doctor = new Doctor(resultSet.getString(2), resultSet.getInt(1));
                list.add(doctor);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return list;
    }*/

}
