package database;

import java.sql.*;


public class DB_TransactionDemo
{
	static int numberOfThreads=10;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection connection = getDatabaseConnection();
		try {
			Statement statement= connection.createStatement();
			statement.executeUpdate("update bank set amount=10 where kontonr=1;");
			closeDatabaseConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int counter=0;
		while(counter<numberOfThreads){
			Worker worker = new Worker();
			worker.start();
			counter++;
		}
	}

	static void closeDatabaseConnection(Connection conn) {
		try
		{
			if (conn != null)
			{
				conn.close();
			}
		}
		catch (SQLException e)
		{

		}
	}

	static Connection getDatabaseConnection(){
		Connection connection=null;
		String dataBase ="jdbc:mysql://localhost:8889/test";
		String userName="root";
		String passWord="root";
		try {
			connection = DriverManager.getConnection(dataBase, userName, passWord);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	static class Worker extends Thread {

		public void run(){
			//System.out.println("hej thread number "+this.getId());
			Connection connection = getDatabaseConnection();
			try {
				updateDatabase(connection);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		private void updateDatabase(Connection connection) throws SQLException {
			
			Statement statement= connection.createStatement();
			statement.execute("begin;");
			statement.execute("select amount from bank where kontonr=1 for update");
			//statement.execute("select amount from bank where kontonr=1");
			ResultSet resultset=statement.getResultSet();
			resultset.next();
			double beloeb=resultset.getInt(1);
			beloeb=beloeb*1.1;
			//statement.execute("update bank set amount="+beloeb+" where kontonr=1;");
			statement.execute("update bank set amount = amount * 1.1 where kontonr=1;");
		    statement.execute("commit;");
			System.out.println("amount is: "+beloeb);

			closeDatabaseConnection(connection);
		}
	}

}
