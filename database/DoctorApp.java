package database;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by joneikholm on 25-10-15.
 */
public class DoctorApp extends Application
{
    //  Database credentials
    private Connection conn = null;
    BorderPane borderPane;
    VBox bookingVBox;
    VBox appointmentCountVBox;
    private ArrayList<Doctor> doctors;
    private ComboBox comboBox;

    public static void main(String[] args)
    {
        launch(args);
    }

    public DoctorApp()
    {

        try
        {
            String DB_URL = "jdbc:mysql://localhost:8889/dat15w";
            String USER = "root";
            String PASS = "root";
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("conn obj created" + conn + " message: ");

            //writeToDB("persons", "per", "123");
            //deleteFromDB("persons", 4);
            //updateInDB("persons", 3, "Ragnhild", "goodday");
            // System.out.println(getPerson("anna"));

        } catch (SQLException e)
        {
            System.out.println("db error" + e.getMessage());
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        appointmentCountVBox = new VBox(5);
        doctors = getDoctors();
        System.out.println("Doc size: " + doctors.size());

        ObservableList<Doctor> options = FXCollections.observableArrayList();

        for (int i = 0; i < doctors.size(); i++)
        {
            options.add(doctors.get(i));

        }
        comboBox = new ComboBox(options);

        borderPane = new BorderPane();
        borderPane.setBottom(appointmentCountVBox);
        bookingVBox = new VBox(5);
        DatePicker datePicker = new DatePicker();
        bookingVBox.getChildren().add(datePicker);
        TextField nameTextField = new TextField();
        nameTextField.setPromptText("enter name");
        TextField dateTextField = new TextField();
        dateTextField.setPromptText("yyyy-mm-dd hh:mm:ss");
        Button bookButton = new Button("Book");
        bookButton.setOnAction(event -> {
            String name = nameTextField.getText();
            String date = dateTextField.getText();
            if (name != null && date != null)
            {
                Doctor doc = (Doctor) comboBox.getSelectionModel().getSelectedItem();
                // will get the actual Doctor object
                insertBooking(name, datePicker.getValue().toString()+ " 10:00:00", doc.id);
                updateAppointmentCount();
                System.out.println(datePicker.getValue());
            }

        });

        bookingVBox.getChildren().addAll(nameTextField, dateTextField, comboBox, bookButton);
        borderPane.setTop(bookingVBox);
        Scene scene = new Scene(borderPane, 600, 600);
        updateAppointmentCount();
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    private void updateAppointmentCount()
    {
        String sql = "SELECT name, count(doctorbooking.idDoctor) FROM doctorbooking\n" +
                "INNER JOIN doctors ON\n" +
                "doctors.idDoctor = doctorbooking.idDoctor\n" +
                "GROUP BY doctorbooking.idDoctor;";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();
            appointmentCountVBox.getChildren().clear();
            Label label = new Label( "Name: \t\t\t\t Number of appointments:");
            appointmentCountVBox.getChildren().add(label);
            while (resultSet.next())
            {
                 label = new Label(resultSet.getString(1) + " \t\t\t\t" + resultSet.getInt(2));
                appointmentCountVBox.getChildren().add(label);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

    }

    private void updateInDB(String table, int id, String changedName, String newPassword)
    {
        String sql = "UPDATE " + table + " SET name=?, password=? WHERE id = ?";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, changedName);
            preparedStatement.setString(2, newPassword);
            preparedStatement.setInt(3, id);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed update. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void deleteFromDB(String table, int id)
    {
        String sql = "DELETE FROM persons WHERE id = ?";
        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed delete. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void insertBooking(String name, String date, int docId)
    {
        String sql = "INSERT INTO doctorbooking VALUES (null, ?, ?, null, ?)";

        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, date);
            preparedStatement.setInt(3, docId);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed insert. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private void writeToDB(String table, String name, String pw)
    {
        String sql = "INSERT INTO persons VALUES (null, ?, ?)";

        try
        {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, pw);
            int numberOfRows = preparedStatement.executeUpdate();
            System.out.println("Completed insert. Number of rows affected:" + numberOfRows);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public String getPerson(String name)
    {
        String out = "";
        try
        {
            String sql = "SELECT * FROM persons WHERE name = ?";

            //Statement statement=conn.createStatement();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
            {
                out = resultSet.getString(1);
                out += " - " + resultSet.getString(2);
                out += " - " + resultSet.getString(3);

            } else
            {
                out = " no data";
            }

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return out;
    }

    public ArrayList<Doctor> getDoctors()
    {
        ArrayList<Doctor> list = new ArrayList<>();
        try
        {
            String sql = "SELECT * FROM doctors";

            //Statement statement=conn.createStatement();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                Doctor doctor = new Doctor(resultSet.getString(2), resultSet.getInt(1));
                list.add(doctor);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return list;
    }

}
