package ListSetMap;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by joneikholm on 21/09/15.
 */
public class UseSet
{
    public static void main(String[] args) {
        Set<String> hardWorkers=new HashSet<>();
        Set<String> gifted=new HashSet<>();
        Set<String> successful=hardWorkers;
        Set<String> everybody=hardWorkers;
        Set<String> hardWorkers_notGifted=hardWorkers;
        hardWorkers.add("Gertz");
        hardWorkers.add("Jakob");
        hardWorkers.add("Tom Cruise");
        hardWorkers.add("Anthony Robbins");
        gifted.add("Gabor");
        gifted.add("Tadas");
        gifted.add("Tom Cruise");
        gifted.add("Martin");
        //successful.retainAll(gifted);
        //System.out.println("Successful:"+successful.toString());
        //everybody.addAll(gifted);
       // System.out.println("Everybody:"+everybody.toString());
        hardWorkers_notGifted.removeAll(gifted);
        System.out.println(hardWorkers_notGifted);


    }
}
