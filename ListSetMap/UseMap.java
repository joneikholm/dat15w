package ListSetMap;

import java.util.HashMap;

/**
 * Created by joneikholm on 21/09/15.
 */
public class UseMap
{
    public static void main(String[] args) {
       /* HashMap<String,Integer> hashMap = new HashMap<>();
        hashMap.put("car",3);// increment this by 1, whenever "car" is found
        hashMap.put("bike",0);
        hashMap.put("plane",7);
        hashMap.put("ship",11);
        hashMap.put("rocket",9);

        String wordFromFile="car";
        if(wordFromFile.equals("car")){
            hashMap.put(wordFromFile,hashMap.get(wordFromFile)+1);
        }

        for(Map.Entry<String, Integer> entry : hashMap.entrySet()){
                if(entry.getValue()==11){
                    System.out.println("We found the element with 11, and key was:"+entry.getKey());
                }
        }
*/
        //Name and age
        HashMap<String, Person> personMap = new HashMap<>();
        personMap.put("aksel",new Person("aksel",1));
        personMap.put("per", new Person("per",21));
        personMap.put("Anna", new Person("Anna",26));
        personMap.put("Peter", new Person("Peter",51));

        System.out.println(personMap.get("Peter").age);



    }
}
