package ListSetMap;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by joneikholm on 21/09/15.
 */
public class UseArrayList
{

    public static void main(String[] args) {
        LinkedList<Integer> integers = new LinkedList<>();
        integers.add(5);
        integers.add(6);
        integers.add(8);

        removeAll(integers, 6);
        superMethod(integers);
        //System.out.println(names.toString());

    }
    public static void superMethod(Collection<Integer> collection){
        Iterator<Integer> iterator = collection.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    public static void removeAll(LinkedList<Integer> list, int value){
        Iterator<Integer> integerIterator = list.iterator();
        while(integerIterator.hasNext()){
            if(integerIterator.next()==value){
              integerIterator.remove();
            }
        }
    }
}
