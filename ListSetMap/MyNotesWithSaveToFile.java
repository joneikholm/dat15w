package ListSetMap;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.*;
import java.util.TreeMap;

/**
 * Created by joneikholm on 04-10-15.
 */
public class MyNotesWithSaveToFile extends Application
{
    BorderPane borderPane;
    VBox editVBox = new VBox(5);
    VBox keyVBox = new VBox(5);
    TextArea editArea = new TextArea();
    TextField keyField = new TextField();
    private Button saveButton;
    private String rootPath = System.getProperty("user.dir");
    private String myNotePath = rootPath + "/myNote.ser";
    private TreeMap<String, String> map;// = new TreeMap<>();
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("My Personal Notebook");
        map = (TreeMap<String, String>) loadObjectFromFile(myNotePath);
        if(map!=null){
            System.out.println("map loaded OK");
        }else {
            map= new TreeMap<>();  // the first time the app is launched.
        }

        keyVBox.setMinWidth(100);
        saveButton = new Button("Save");
        editVBox.getChildren().addAll(keyField,editArea,saveButton);
        Button addButton = new Button("New note");
        addButton.setOnAction(event -> {
            editArea.clear();
            keyField.clear();
            borderPane.setCenter(editVBox);
            addButton.setDisable(true);  // disable addButton while we edit
        });

        saveButton.setOnAction(event -> {
            map.put(keyField.getText(), editArea.getText()); //add to map
            updateButtons();
            borderPane.setCenter(null);  // clear center
            addButton.setDisable(false);  // enable addButton
        });
        borderPane = new BorderPane();
        borderPane.setLeft(keyVBox);
        borderPane.setTop(addButton);
        updateButtons();
        Scene scene = new Scene(borderPane,450,600);
        primaryStage.setOnCloseRequest(event -> {
            event.consume();
            saveObjectToFile(myNotePath, map);
            primaryStage.close();
        });
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void updateButtons() {
        keyVBox.getChildren().clear();
        for(String key:map.keySet()){ //add buttons to left panel, do this by iterating all keys in map
            Button button =new Button(key);
            button.setOnAction(event1 -> {
                keyField.setText(button.getText());
                editArea.setText(map.get(button.getText()));
                borderPane.setCenter(editVBox);
            });
            keyVBox.getChildren().add(button);
        }
    }

    private void saveObjectToFile(String filename, Object object) {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(object);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }

    }
    private Object loadObjectFromFile(String filename) {
        Object object = null;
        try {
            File file = new File(filename);
            if (file.exists()) {
                FileInputStream fileIn = new FileInputStream(filename);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                object = in.readObject();
                in.close();
                fileIn.close();
                return object;
            } else {
                return null;
            }
        } catch (IOException i) {
            i.printStackTrace();
            return null;
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
            return null;
        }
    }
}
