package ListSetMap;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by joneikholm on 05-10-15.
 */
public class MyNotes extends Application
{

    BorderPane borderPane;
    VBox leftColumnButtonsVBox;
    VBox centerContentVBox;
    TextField titleField;
    TextArea textArea;
    TreeMap<String, String> map = new TreeMap<>();  //start with empty map for now
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        borderPane = new BorderPane();
        leftColumnButtonsVBox= new VBox(3);
        centerContentVBox= new VBox(3);
        titleField = new TextField();
        textArea = new TextArea();
        Button saveButton = new Button("Save");
        centerContentVBox.getChildren().addAll(titleField,textArea,saveButton);
        saveButton.setOnAction(event -> {
            // store the text along with the title
            map.put(titleField.getText(), textArea.getText());
            updateButtons();
            borderPane.setCenter(null); // clears center area
        });
        //borderPane.setCenter(centerContentVBox); // fills center area
        borderPane.setLeft(leftColumnButtonsVBox);
        // + button, for creating a new entry
        Button newNoteButton = new Button("New Note");
        newNoteButton.setOnAction(event -> {
            titleField.clear();
            textArea.clear();
            borderPane.setCenter(centerContentVBox);
        });
        borderPane.setTop(newNoteButton);
        Scene scene = new Scene(borderPane,450,600);
        primaryStage.setTitle("My Cool Note App");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private void updateButtons(){
        leftColumnButtonsVBox.getChildren().clear();
        for(Map.Entry<String, String> entry : map.entrySet()){
            Button entryButton = new Button(entry.getKey());
            entryButton.setOnAction(event1 -> {
                // somehow get the text which corresponds to the "key", which was
                // the text on the button
                titleField.setText(entry.getKey());
                textArea.setText( entry.getValue());
                borderPane.setCenter(centerContentVBox); // fills center area
            });
            leftColumnButtonsVBox.getChildren().add(entryButton);
        }
    }
}
