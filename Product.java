/**
 * Created by joneikholm on 11/09/15.
 */
public class Product
{
    // this class will be a Java Bean IF we also
    // implement Serializable

    public Product(String n, double p){
        name=n;
        price=p;
    }

    public String getName() {// HAS to be this style
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    private String name;
    private double price;

}
