import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by joneikholm on 14/09/15.
 */
public class Worker implements Runnable
{
    private final String USER_AGENT="Mozilla/5.0";

    private LoginFX loginFX;

    public Worker(LoginFX loginFX){
        this.loginFX=loginFX;
    }

    @Override
    public void run() {
        sendGETRequest();
    }



    private void sendGETRequest(){
        String url ="http://joneikholm.dk/checkuser.php?username=Julio&password=Iglesias";
        StringBuffer response = null;
        try{
            URL urlObject = new URL(url);
            HttpURLConnection con =(HttpURLConnection)urlObject.openConnection();
            con.setRequestMethod("GET");  // GET is default... could be POST instead
            con.setRequestProperty("User-Agent", USER_AGENT);
            int responsecode = con.getResponseCode();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            response=new StringBuffer();
            while ((inputLine=in.readLine())!=null){
                response.append(inputLine);
            }
            in.close();
            System.out.println("Response from server is:" + response.toString());
            System.out.println("responsecode is:"+responsecode);
            loginFX.updateLabel(response.toString());
        }catch (Exception e){
            System.out.println("error: "+e.getMessage());
            loginFX.updateLabel(e.getMessage());
        }
    }
}
